# Sistem Pendukung Keputusan metode AHP

## Ruby
    ruby 2.3.3p222 (2016-11-21 revision 56859) [i386-mingw32]

## Database
    Tested on mysql Ver 15.1 Distrib 10.1.19-MariaDB, for Win32 (AMD64)

## Gem
    bootstrap-sass
    font-awesome-sass
    chartkick
    devise
    icheck-rails
